using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwitch : MonoBehaviour
{
    public Camera Cam1;
    public Camera Cam2;
    public Camera Cam3;

    private Camera[] listecameras = new Camera[3];
    private int currentIndex;

    // Start is called before the first frame update
    void Start()
    {
        currentIndex = 0;
        listecameras[0] = Cam1;
        Cam1.enabled = true;
        listecameras[1] = Cam2;
        Cam2.enabled = false;
        listecameras[2] = Cam3;
        Cam3.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.Tab)){
            int newIndex = (currentIndex + 1) % 3;
            listecameras[currentIndex].enabled = false;
            listecameras[newIndex].enabled = true;
            currentIndex = newIndex;            
        }
    }
}
