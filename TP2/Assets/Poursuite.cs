using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Poursuite : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 S = GameObject.Find("Joueur").transform.position;
        Vector3 C = transform.position; 
        float v = 0.01f;
        Vector3 SC = S-C;
        float norm = SC.magnitude;

        this.transform.Translate(v * SC / norm);
        Quaternion rotation = Quaternion.LookRotation(-SC, Vector3.up);
        transform.rotation = rotation;
    }
}
