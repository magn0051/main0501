using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mouvement : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.DownArrow))
        {
            //if shift is pressed, moves forward faster
            if(Input.GetKey(KeyCode.LeftShift)){
                transform.Translate(Vector3.forward *  0.03f);
            } else{
                transform.Translate(Vector3.forward * 0.01f);
            }
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            //same as moving forward, shift makes it faster
             if(Input.GetKey(KeyCode.LeftShift)){
                transform.Translate(Vector3.back * 0.03f);
            } else{
                transform.Translate(Vector3.back * 0.01f);
            }
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(Vector3.up, -1);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(Vector3.up, 1);
        }
        //pressing space makes the character "jump"
        if (Input.GetKey(KeyCode.Space))
        {
            //up changes the y axis
            transform.Translate(Vector3.up * 0.03f);
        }

        //punch mechanic
        if(Input.GetMouseButtonDown(0))
        {
            var bras = transform.GetChild(2).GetChild(0);
            Vector3 rotation = new Vector3(0,0,90);
            bras.transform.rotation = Quaternion.Slerp(bras.transform.rotation,Quaternion.Euler(rotation), 0.01f);
            //bras.transform.RotateAround(bras.transform.position, Vector3.back, 20 * Time.deltaTime);
        }

    }
}
